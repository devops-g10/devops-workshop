package resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.regex.Pattern;

/**
 * Calculator resource exposed at '/calculator' path
 */
@Path("/calculator")
public class CalculatorResource {
    /**
     *Addisjon: ^\\d+(?:\\s*[+]\\s*\\d+)*$
     * Substraksjon: ^\\d+(?:\\s*[-]\\s*\\d+)*$
     * Multiplikasjon: ^\\d+(?:\\s*[*]\\s*\\d+)*$
     * Divisjon: ^\\d+(?:\\s*[/]\\s*\\d+)*$
     *
     */

    private final static Pattern ADDITION_PATTERN = Pattern.compile("^\\d+(?:\\s*[+]\\s*\\d+)*$");
    private final static Pattern SUBTRACTION_PATTERN = Pattern.compile("^\\d+(?:\\s*[-]\\s*\\d+)*$");
    private final static Pattern MULTIPLICATION_PATTERN = Pattern.compile("^\\d+(?:\\s*[*]\\s*\\d+)*$");
    private final static Pattern DIVISION_PATTERN = Pattern.compile("^\\d+(?:\\s*[/]\\s*\\d+)*$");


    /**
     * Method handling HTTP POST requests. The calculated answer to the expression will be sent to the client as
     * plain/text.
     * @param expression the expression to be solved as plain/text.
     * @return solution to the expression as plain/text or -1 on error
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String calculate(String expression){
        // Removes all whitespaces
        String expressionTrimmed = expression.replaceAll("\\s+","");

        int result = -1;

        /*
         * .matches use regex expression to decide if a String matches a given pattern.
         * [0-9]+[+][0-9]+ explained:
         * [0-9]+: a number from 0-9 one or more times. For example 1, 12 and 123.
         * [+]: the operator + one time.
         * The total regex expression accepts for example:
         * 12+34,
         * 1+2,
         * 10000+1000
         */
        if (ADDITION_PATTERN.matcher(expression).matches()) result = sum(expressionTrimmed);
        else if (SUBTRACTION_PATTERN.matcher(expression).matches()) result = subtraction(expressionTrimmed);
        else if (MULTIPLICATION_PATTERN.matcher(expression).matches()) result = multiplication(expressionTrimmed);
        else if (DIVISION_PATTERN.matcher(expression).matches()) result = division(expressionTrimmed);

        if (result == -1) {
            return "Invalid expression. Sad monke 🍌";
        }

        return expression + "=" + result;
    }

    /**
     * Method used to calculate a sum expression.
     * @param expression the equation to be calculated as a String
     * @return the answer as an int
     */
    public int sum(String expression){
        String[] split = expression.split("[+]");

        int sum = 0;
        for (String str : split) {
            sum += Integer.parseInt(str);
        }

        return sum;
    }

    /**
     * Method used to calculate a subtraction expression.
     * @param expression the expression to be calculated as a String
     * @return the answer as an int
     */
    public int subtraction(String expression){
        String[] split = expression.split("[-]");

        int num = Integer.parseInt(split[0]);
        for (int i = 1; i < split.length; i++) {
            num -= Integer.parseInt(split[i]);
        }

        return num;
    }

    public int multiplication(String expression){
        String[] split = expression.split("[*]");

        int num = Integer.parseInt(split[0]);
        for (int i = 1; i < split.length; i++) {
            num *= Integer.parseInt(split[i]);
        }

        return num;
    }

    public int division(String expression){
        String[] split = expression.split("[/]");

        int num = Integer.parseInt(split[0]);
        for (int i = 1; i < split.length; i++) {
            num /= Integer.parseInt(split[i]);
        }

        return num;
    }
}
