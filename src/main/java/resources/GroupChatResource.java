package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;
import data.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId) {
        GroupChatDAO dao = new GroupChatDAO();
        return dao.getGroupChat(groupChatId);
    }

    @GET
    @Path("user/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatsByUserId(@PathParam("userId") int userId) {
        GroupChatDAO dao = new GroupChatDAO();
        return dao.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat) {
        GroupChatDAO dao = new GroupChatDAO();
        return dao.addGroupChat(groupChat);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessages(@PathParam("groupChatId") int groupChatId) {
        GroupChatDAO dao = new GroupChatDAO();
        return dao.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {
        GroupChatDAO dao = new GroupChatDAO();
        return dao.addMessage(groupChatId, message);
    }
}
