import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("100+300=400", calculatorResource.calculate(expression));

        expression = "300-99";
        assertEquals("300-99=201", calculatorResource.calculate(expression));

        expression = "200*2";
        assertEquals("200*2=400", calculatorResource.calculate(expression));

        expression = "400/2";
        assertEquals("400/2=200", calculatorResource.calculate(expression));

        expression = "100+200+53";
        assertEquals("100+200+53=353", calculatorResource.calculate(expression));

        expression = "353-200-53";
        assertEquals("353-200-53=100", calculatorResource.calculate(expression));

        expression = "5*10*2";
        assertEquals("5*10*2=100", calculatorResource.calculate(expression));

        expression = "100/2/10";
        assertEquals("100/2/10=5", calculatorResource.calculate(expression));

        expression = "no banana";
        assertEquals("Invalid expression. Sad monke 🍌", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "100+200+53";
        assertEquals(353, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "353-200-53";
        assertEquals(100, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "25*2";
        assertEquals(50, calculatorResource.multiplication(expression));

        expression = "10*3";
        assertEquals(30, calculatorResource.multiplication(expression));

        expression = "5*10*2";
        assertEquals(100, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "500/2";
        assertEquals(250, calculatorResource.division(expression));

        expression = "20/2";
        assertEquals(10, calculatorResource.division(expression));

        expression = "100/2/10";
        assertEquals(5, calculatorResource.division(expression));
    }
}
